import React from 'react'

export default function TableComponent({person}) {
  return (

<div>
      <br />
      <div className="relative">
        <table className="text-sm text-left text-white w-10/12 mx-32">
          <thead className="text-xs uppercase">
            <tr className="text-black bg-yellow-200">
              <th scope="col" className="px-6 py-3">
                ID
              </th>
              <th scope="col" className="px-6 py-3">
              Username
              </th>
              <th scope="col" className="px-6 py-3">
                Email
              </th>
              <th scope="col" className="px-6 py-3">
                Age
              </th>
            </tr>
          </thead>
          <tbody>  {person.map((p)=> (
             <tr key={p.id} className="bg-white dark:bg-gray-800">
             <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
               {p.id}
             </th>
             <td className="px-6 py-4">
               {p.name}
             </td>
             <td className="px-6 py-4">
               {p.email}
             </td>
            
             <td className="px-6 py-4">
               {p.age}
             </td>
           </tr>
          ) ) }
           
          </tbody>
        </table>
      </div>
    </div>
  )
}
