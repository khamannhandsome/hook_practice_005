import React, { useState } from 'react'
import TableComponent from './TableComponent'

export default function InputComponent(props) {

// const [newPerson, setNewPerson] = useState({})

// const handleInput = (i) =>{
//   const {name, value}= i.target
//   setNewPerson({...newPerson,[name]:value})
// }

// const handleSubmit = () => {
//   prop.setPerson([...prop.person, newPerson]);
//   setNewPerson({}) 
// }

const [newPerson, setNewPerson] = useState({ name: '', email: '', age: '' });

const handleInput = (e) => {
  const { name, value } = e.target;
  setNewPerson ({ ...newPerson, [name]: value });
};


const handleSubmit = (e) => {
  e.preventDefault();
  const newId = props.person.length + 1;
  const newPersonWithId = { ...newPerson, id: newId };
  props.setPerson([...props.person, newPersonWithId]);
  setNewPerson({ name: '', email: '', age: '' });
};

  return (
    <div>
      
    <p className="text-3xl text-pink-300 font-bold">PLEASE PROVIDE US YOUR INFORMATION❤️</p>

<br />
    <label for="input-group-1" class="mx-48 block mb-2 text-sm font-medium text-start">Username</label>
<div class="flex mx-48">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0">
  💌
  </span>
  <input type="text" id="website-admin" value={newPerson.name} onChange={handleInput} name='name'   class="rounded-none rounded-r-lg bg-gray-50 border  flex-1 min-w-0 w-full text-sm  p-2.5 " placeholder="Your name"/>
</div> <br />


<label for="input-group-1" class="block mb-2 text-sm font-medium text-start mx-48">Email</label>
<div class="flex mx-48">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0">
    🏳️
  </span>
  <input type="text" id="website-admin" value={newPerson.email}  onChange={ handleInput} name='email' class=" rounded-none rounded-r-lg bg-gray-50 border  flex-1 min-w-0 w-full text-sm  p-2.5 " placeholder="example@gmail.com"/>
 
</div> <br />


<label for="website-admin" class="mx-48 block mb-2 text-sm font-medium text-start">Age</label>
<div class="flex mx-48">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0">
  ☯️
  </span>
  <input type="text" id="website-admin" value={newPerson.age}  onChange={handleInput} name='age'  class="rounded-none rounded-r-lg bg-gray-50 border  flex-1 min-w-0 w-full text-sm  p-2.5 " placeholder="Your age"/>
</div>
  <br />
<button type="button" onClick={handleSubmit} class="text-white bg-gradient-to-r from-pink-400 via-pink-500 to-pink-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-pink-300 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-16 py-4 text-center mr-2 mb-2 ">Submit</button>


    </div>
  )
}
