import './App.css';
import InputComponent from './MyComponents/InputComponent';
import TableComponent from './MyComponents/TableComponent';

import React, { useState } from 'react';

function App() {
  const [person, setPerson]= useState([{id:1, name:'Kha Mann', email:'khamann@gmail.com',age:99}])


  return (
    <div className="App">


  <InputComponent person={person} setPerson={setPerson} />
  <TableComponent person={person} />


    </div>
  );
}

export default App;
